import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { compose } from "ramda";

import Table from "./components/Table";
import Modal from "./components/Modal";

import {
  parse,
  tokenize,
  generateTableValues,
  generateTableContents
} from "./utils";

import styles from "./App.module.css";

const getValues = compose(
  generateTableContents,
  generateTableValues,
  parse,
  tokenize
);

class App extends Component {
  state = {
    operation: "",
    headers: [],
    rows: [],
    errors: []
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  generateTable = () => {
    const { operation } = this.state;
    if (operation !== "") {
      const { headers, rows, error } = getValues(operation);
      if (error !== undefined) {
        this.handleError(error);
        this.resetState();
      } else {
        this.setState({ tableHeaders: headers, tableRows: rows });
      }
    } else {
      this.handleError("Ingrese una operación");
      this.resetState();
    }
  };

  resetState = () => {
    this.setState({ operation: "", tableHeaders: null, tableRows: null });
  };

  handleError = error => {
    toast.error(`🦄 ${error}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  };
  render() {
    return (
      <>
        <ToastContainer />
        <Modal className={styles.modal} />
        <div className={styles.app}>
          <Paper className={styles.card}>
            <div className={styles.container}>
              <TextField
                className={styles.input}
                label="Ingrese una operación"
                value={this.state.operation}
                onChange={this.handleChange("operation")}
              />
              <Button
                variant="contained"
                color="primary"
                className={styles.generateButton}
                onClick={this.generateTable}
              >
                Generar tabla
              </Button>
              {this.state.tableHeaders && this.state.tableRows ? (
                <Button
                  className={styles.resetButton}
                  onClick={this.resetState}
                >
                  Reiniciar
                </Button>
              ) : null}
              <div className={styles.table}>
                <Table
                  headers={this.state.tableHeaders}
                  rows={this.state.tableRows}
                />
              </div>
            </div>
          </Paper>
        </div>
      </>
    );
  }
}

export default App;
