import React from "react";
import {
  Table as MaterialTable,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from "@material-ui/core";

import styles from "./Table.module.css";

const Table = ({ headers, rows }) => {
  return headers && rows ? (
    <MaterialTable className={styles.table}>
      <TableHead>
        <TableRow>
          {headers.map((header, i) => (
            <TableCell className={styles.tableCell} key={i}>
              {header}
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map(columnValues => (
          <TableRow key={columnValues} className={styles.tableRow}>
            {columnValues.map((val, i) => (
              <TableCell key={i} className={styles.tableCell}>
                {val === false ? "F" : "V"}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </MaterialTable>
  ) : null;
};

export default Table;
