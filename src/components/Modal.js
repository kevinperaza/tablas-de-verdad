import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import Grid from "@material-ui/core/Grid";

import styles from "./Modal.module.css";

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const Modal = ({ buttonText, title, modalContent }) => {
  const [state, setState] = useState({ open: false });
  const handleClickOpen = () => setState({ open: true });
  const handleClose = () => setState({ open: false });
  return (
    <div>
      <Button
        className={styles.modalButton}
        variant="contained"
        color="secondary"
        onClick={handleClickOpen}
      >
        Información
      </Button>
      <Dialog
        open={state.open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          Información adicional
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <Grid container justify="center" spacing={24}>
                  <Grid item>
                    <h3>Operaciones soportadas</h3>
                    <ul>
                      <li>
                        Negación (<code>!, ~, ¬</code>)
                      </li>
                      <li>
                        Conjunción (<code>^ , &</code>)
                      </li>
                      <li>
                        Disyunción (<code>∨ , |</code>)
                      </li>
                      <li>
                        Condicional (<code>→</code>)
                      </li>
                      <li>
                        Bi-condicional (<code>↔</code>)
                      </li>
                    </ul>
                  </Grid>
                  <Grid item>
                    <h3>Orden de evaluación</h3>
                    <ul>
                      <li>Expresiones entre parentesis</li>
                      <li>Negación</li>
                      <li>Conjunción</li>
                      <li>Disyunción</li>
                      <li>Condicional</li>
                      <li>Bi-condicional</li>
                    </ul>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default Modal;
