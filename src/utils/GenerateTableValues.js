import { isEmpty } from "ramda";

export const generateTableValues = obj => {
  const symbols = obj.symbols || {};
  return { ...obj, tableValues: [...valueGenerator(symbols)] };
};

const valueGenerator = symbols => {
  let key, tmp, prev, tableValues;
  if (isEmpty(symbols)) {
    return [{}];
  }
  //TODO: hacer esto menos feo.
  Object.keys(symbols).forEach((k, i) => {
    key = k;
  });

  if (isEmpty(key)) {
    return [{}];
  }

  tmp = Object.assign({}, symbols);
  delete tmp[key];
  prev = valueGenerator(tmp);

  tableValues = [];
  //TODO: hacer esto menos feo.
  Object.keys(prev).forEach((k, i) => {
    let current = Object.assign({}, prev[i]);
    current[key] = true;
    tableValues.push(current);
    current = Object.assign({}, prev[i]);
    current[key] = false;
    tableValues.push(current);
  });

  return tableValues || {};
};

export default generateTableValues;
