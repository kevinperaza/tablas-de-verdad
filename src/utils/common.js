export const DEBUG = false;
export const SYMBOL = /[a-zA-Z]\w*/;
export const WHITESPACE = /^\s*$/;

export const debug = args => {
  if (DEBUG && window.console && window.console.log) {
    console.log(args);
  }
};

export const log = val => {
  console.log(val);
  return val;
};
