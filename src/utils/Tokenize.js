import { isEmpty, filter, compose, map, flatten } from "ramda";
import { SYMBOL, WHITESPACE } from "./common";

const stringCleaner = str => {
  const cleanString = str.replace(/\s/g, "");
  return !SYMBOL.test(cleanString) ? cleanString.split("") : cleanString;
};

const getTokens = compose(
  flatten,
  filter(String),
  map(stringCleaner)
);

const tokenize = str =>
  isEmpty(str) || WHITESPACE.test(str)
    ? {}
    : {
        expression: str,
        tokens: getTokens(str.split("")),
        tableValues: [],
        ast: [],
        errors: []
      };

export default tokenize;
