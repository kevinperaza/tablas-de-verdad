import { debug, SYMBOL } from "./common";

const parse = (() => {
  let pos;
  let symbols;
  let tokens;
  let error = "";

  const getCurrentToken = () => tokens[pos];

  const consumeSymbol = () => {
    debug("consumeSymbol");
    const symbol = consumeToken();
    symbols[symbol] = true;
    return symbol;
  };

  const consumeToken = expected => {
    const currentToken = getCurrentToken();
    debug("consumeToken", currentToken, pos);
    if (expected && currentToken !== expected) {
      error =
        'No se encontro el token esperado. Esperado: "' +
        expected +
        '" Recibido: "' +
        currentToken +
        '".';
    }
    pos++;
    return currentToken;
  };

  const buildAST = () =>
    getCurrentToken() === "!" ||
    getCurrentToken() === "~" ||
    getCurrentToken() === "¬"
      ? [consumeToken(), binaryExpr()]
      : conditionalExpr();

  const binaryExpr = () => conditionalExpr();

  const conditionalExpr = () => {
    var a1 = biConditionalExpr();
    return getCurrentToken() === "→"
      ? [consumeToken(), a1, conditionalExpr()]
      : a1;
  };

  const biConditionalExpr = () => {
    var a1 = orExpr();
    return getCurrentToken() === "↔"
      ? [consumeToken(), a1, conditionalExpr()]
      : a1;
  };

  const orExpr = () => {
    const a1 = andExpr();
    return getCurrentToken() === "∨" || getCurrentToken() === "|"
      ? [consumeToken(), a1, orExpr()]
      : a1;
  };

  const andExpr = () => {
    const a1 = subExpr();
    return getCurrentToken() === "^" || getCurrentToken() === "&"
      ? [consumeToken(), a1, andExpr()]
      : a1;
  };

  const subExpr = () => {
    debug("subExpr", tokens, pos);
    if (getCurrentToken() === "(") {
      consumeToken("(");
      const ret = buildAST();
      consumeToken(")");
      return ret;
    }

    return SYMBOL.test(getCurrentToken()) ? consumeSymbol() : buildAST();
  };

  return obj => {
    if (error) {
      error = "";
    }
    const { tokens: tok } = obj;
    debug("parse", tok);
    if (!tok || !tok.length) {
      return [];
    }

    tokens = tok;
    pos = 0;
    symbols = {};
    const ast = buildAST();
    if (pos < tokens.length) {
      debug(tokens);
      debug(pos);
      debug(tokens[pos]);
      error =
        "No se logro consumir todos los tokens. " +
        "Tokens restantes: " +
        tokens.slice(pos, tokens.length);
    }
    return error
      ? { error: error }
      : {
          ...obj,
          ast: ast,
          symbols: symbols
        };
  };
})();

export default parse;
