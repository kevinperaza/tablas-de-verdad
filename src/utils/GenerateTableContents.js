import { debug } from "./common";

const evalExpr = (ast, bindings) => {
  const evalSym = index => {
    if (Array.isArray(ast[index])) {
      return evalExpr(ast[index], bindings);
    }

    assertBoolean(bindings[ast[index]]);
    return bindings[ast[index]];
  };

  const isBoolean = val => {
    return val === true || val === false;
  };

  const assertBoolean = val => {
    if (!isBoolean(val)) {
      return { error: "Simbolo sin unir: " + val };
    }
  };

  if (!ast) {
    return { error: "Expresion invalida: " + ast };
  }

  if (!Array.isArray(ast)) {
    return bindings[ast];
  }

  switch (ast[0]) {
    case "^":
    case "&":
      return evalSym(1) && evalSym(2);
    case "↔":
      return evalSym(1) === evalSym(2);
    case "→":
      return !evalSym(1) || evalSym(2);
    case "∨":
    case "|":
      return evalSym(1) || evalSym(2);
    case "!":
    case "¬":
    case "~":
      return !evalSym(1);
    default:
      return { error: "No se reconoce el operador: " + ast[0] };
  }
};

export const generateTableContents = obj => {
  const { expression, ast, symbols, tableValues, error } = obj;
  debug(tableValues);
  if (error !== undefined) {
    return { error: error };
  }
  if (symbols) {
    const response = {
      headers: [...Object.keys(symbols), expression],
      rows: tableValues.map(row => {
        const result = evalExpr(ast, row);
        debug("evaluated", row, result);
        return [...Object.values(row), result];
      })
    };

    const {
      rows: { error }
    } = response;
    return error !== undefined ? { ...response, error: error } : response;
  }
};

export default generateTableContents;
