## JavaScript 101

Que es? [https://es.wikipedia.org/wiki/JavaScript](https://es.wikipedia.org/wiki/JavaScript)

[Completar este tutorial](https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/JavaScript_basics)

## GIT

Que es? [https://es.wikipedia.org/wiki/Git](https://es.wikipedia.org/wiki/Git)

Como usar git pt1? [https://gitexplorer.com/](https://gitexplorer.com/)

Git branching: [https://learngitbranching.js.org/](https://learngitbranching.js.org/)

Instalar GIT de [https://git-scm.com/](https://git-scm.com/)

## SourceTree

Que es? [https://www.sourcetreeapp.com/](https://www.sourcetreeapp.com/)

Instalar SourceTree siguiendo el tutorial en [https://confluence.atlassian.com/get-started-with-sourcetree/install-sourcetree-847359094.html](https://confluence.atlassian.com/get-started-with-sourcetree/install-sourcetree-847359094.html)

## NodeJS

Que es? [https://es.wikipedia.org/wiki/Node.js](https://es.wikipedia.org/wiki/Node.js)

Instalar NodeJS de [https://nodejs.org/es/](https://nodejs.org/es/)

## VSCode

Que es? [https://es.wikipedia.org/wiki/Visual_Studio_Code](https://es.wikipedia.org/wiki/Visual_Studio_Code)

Instalar VSCode de [https://code.visualstudio.com/](https://code.visualstudio.com/)

## Available Scripts

En el directorio del proyecto, puede correr:

### `yarn install`

Instalar las dependencias del proyecto

### `yarn start`

Corre la aplicacion en modo desarrollo
Abra [http://localhost:3000](http://localhost:3000) para ver la aplicacion en el navegador

The page will reload if you make edits.

You will also see any lint errors in the console.

# Learn More (Opcional pero recomendado)

##JavaScript:
You don't know JS [https://github.com/getify/You-Dont-Know-JS](https://github.com/getify/You-Dont-Know-JS)

JavaScript Design Patterns [https://addyosmani.com/resources/essentialjsdesignpatterns/book/](https://addyosmani.com/resources/essentialjsdesignpatterns/book/)
##ReactJS:
Para aprender mas de ReactJS, ver [documentacion de react](https://reactjs.org/).
